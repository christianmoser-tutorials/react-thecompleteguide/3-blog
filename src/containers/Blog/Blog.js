import React, { Component } from 'react';
import Posts from './Posts/Posts'
import { Route, NavLink, Switch, Redirect } from 'react-router-dom'
import './Blog.css';

//import NewPost from './NewPost/NewPost'
import asyncComponent from '../../hoc/asyncComponent'

const AsyncNewPost = asyncComponent(() => {
  return import('./NewPost/NewPost')
})

class Blog extends Component {
    state = {
      auth: true
    }
    render () {
        return (
            <div className="Blog">
              <header>
                <nav>
                  <ul>
                    <li>
                      <NavLink 
                        to="/posts"
                        activeClassName="my-active"
                        activeStyle={{
                          color: '#fa923f',
                          textDecoration: 'underline'
                        }}
                      >Posts</NavLink></li>
                    <li>
                      <NavLink 
                        exact
                        to={{
                          pathname: '/new-post'
                        }}
                      >
                      New Post
                    </NavLink></li>
                  </ul>
                </nav>
              </header>
              <Switch>
                {this.state.auth ? <Route path="/new-post" exact component={AsyncNewPost}/> : null}
                <Route path="/posts" component={Posts}/>
                {/* <Redirect from="/" to="/posts"/> */}
                <Route render={() => <h1>404</h1>}/>
              </Switch>
            </div>
        );
    }
}

export default Blog;